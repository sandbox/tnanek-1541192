<?php

/**
 * @file
 *   deploy commands for Drush to speed up development workflows.
 */


/**
* Implements hook_drush_command().
*
* @See drush_parse_command() for a list of recognized keys.
*
* @return
*   An associative array describing each command.
*/
function deploy_drush_command() {
  $items = array();

  $items['deploy-code'] = array(
    'description' => "A method of syncing the code behind the various " .
      "versions of a site. By default this copies the dev to the demo for " .
      "testing, and can copy to the production with an argument.",
    'arguments' => array(
      'origin' => "Either the site's general alias for automated ".
        'selection, or option specific selection, or the specific ' .
        "site's alias.",
      'target' => "Optional, can be a site specific alias to copy the " .
        "code to.",
    ),
    'required-arguments' => 1,
    'options' => array(
      'origin' => "Only referred to when the argument isn't specific enough.",
      'target' => "Only referred to when matching arg is not there.",
      'items' => "Which folders or files to replace, defaults to 'modules,themes,".
        "libraries'. These are the subfolders or files within the origin to ".
        "duplicate in the target.",
    ),
    'examples' => array(
      'drush deploy-code @self' => "Will copy the modules,themes,libraries folders from @self.dev to @self.demo for testing with content.",
      'drush deploy-code @self.dev @self.prod' => "Will copy the modules,themes,libraries folders from @self.dev to @self.prod.",
      'drush dec @self --origin=demo --target=prod' => "Will copy the modules, themes and libraries folders from the demo site within the work ".
        "subsites to the production site within the same subset.",
      'drush dec @self --items="modules,drushrc.php,files"' => "Will copy the modules, files directories as well as the drushrc.php from the dev build of the ".
        "@self site to the demo build."
    ),
    'aliases' => array(
      'dec',
    ),
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUSH',
    'scope' => 'system',
  );

  $items['deploy-download'] = array(
    'description' => "Downloads Drupal and sets symlinks so it can be simply updated to.",
    'aliases' => array('ded'),
    'arguments' => array(
      'core' => "A number or an alies to a Drupal install you'd like to replace. This gets the core version from that install and uses that install locations parent folder as the destination.",
    ),
    'required-arguments' => TRUE,
    'examples' => array(
      'drush deploy-download 7' => 'Will download the recommended release of Drupal 7 to your current directory and replace some files with symlinks.',
      'drush ded 6 --symlinks=0' => 'Will download the recommended release of Drupal 6 to your current directory and not replace anyhing with symlinks.',
      'drush ded @example --destination=/var/www/example.com --items=htaccess,php.ini,sites,profiles' => 'Download to the parent directory of an established site and use all pre-configured optional symlinks.',
    ),
    'options' => array(
      'destination' => "If using a number for the core argument, this is a way to specify the location. Will be overridden by the alias if passed as the argument.",
      'symlinks' => 'Whether to establish symlinks or not, defaults to yes.',
      'items' => 'What items to establish symlinks to. Either an array can be provided, with keys being items in the Drupal root, targets being destinations to point to, or a list. List possibilities include htaccess,php.ini,sites,profiles. Defaults to list of sites and profiles.',
      'core' => array(
        'description' => 'Internal option to pass along a value determined from the argument',
        'hidden' => TRUE,
      ),
      'version' => array(
        'description' => 'The specific minor version to download, this will always be the recommended release of the given core version.',
        'hidden' => TRUE,
      ),
    ),
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUSH',
    'scope' => 'system',
  );

  // I always have a command of this sort to speed up the help system.
  $items['deploy'] = array(
    'description' => 'Custom commands to ease deployment workflow.',
    'bootstrap'   => DRUSH_BOOTSTRAP_DRUSH,
    'scope'       => 'system',
    'hidden'      => TRUE,
  );

  return $items;
}


/**
 * Implements hook_drush_help().
 *
 * @param
 *   A string for user output
 *
 * @return
 *   A string with the help text for your command.
 */
function deploy_drush_help($section) {
  switch ($section) {
    // Basic help text
    case 'drush:deploy-code':
      return dt('Duplicate the specified files within the site to another, overwriting those.');
    case 'drush:deploy-download':
      return dt("Downloads the latest version of Drupal, and establishes custom symlinks, as per the options.");
    // Meta data
    case 'meta:deploy:title':
      return dt('Custom commands');
    case 'meta:deploy:summary':
      return dt('Shortcuts to ease developer workflows.');
    // Error codes
    case 'error:DEPLOY_NUMERIC_CORE_DESTINATION':
      return dt('With a numeric core argument, destination is a mandatory option.');
    case 'error:CUSTOM_SETTING_INVALID':
      return dt('Attempted to retrieve an invalid setting');
    case 'error:CUSTOM_SETTING_COMMAND':
      return dt('Attempted to retrieve a setting for a command which was invalid.');
    case 'error:DEPLOYMENT_BACKUP_NOT_FOLDER':
      return dt('Backup argument must be a folder to store the backups inside of.');
    case 'error:DEPLOYMENT_CODE_COPY_REQUIRED':
      return dt('Need something to copy, otherwise this command is useless.');
    case 'error:CUSTOM_DEPLOYMENT_CODE_LACK_ORIGIN':
      return dt('An origin site to get the files from is required.');
    case 'error:CUSTOM_DEPLOYMENT_CODE_INVALID_ORIGIN':
      return dt('Origin alias was not specific enough for use.');
    case 'error:DEPLOY_DOWNLOAD_INVALID_CORE':
      return dt('Invalid core argument provided.');
    case 'error:DEPLOY_DOWNLOAD_DESTINATION_EXIST':
      return dt('Destination argument needs to be an existing folder.');
    case 'error:DEPLOY_DOWNLOAD_RECOMMENDED':
      return dt("This surpasses or is the recommended version, thus there's no point in downloading this.");
    case 'error:DEPLOY_DOWNLOAD_DOWNLOAD_FAILED':
      return dt('Download failed; please check the permissions on the destination folder.');
  }
}


/**
 * Implements drush_COMMANDNAME().
 *
 * All of my drush commandfiles have a command like this.
 */
function drush_deploy() {
  $alias = array('@none');
  $command = 'help';
  $args = array();
  $options[] = '--filter=deploy';
  $results = drush_invoke_process($alias,$command,$args,$options);
  $results = $results['output'];
  drush_print('The following commands are in the deploy package. Use the `drush help <command>` for further information.');
  drush_print($results);
}

/**
 * A way to retieve settings, from either arguments or options. Also formats
 * the options for ease of use later on, and sets them. This is all the
 * preprocessing essentially.
 *
 * Function varies by what the command is, as well as the $name passed.
 *
 * @param $name = NULL
 *  What setting to get. Most will load the arguments prior to the option, and
 *  always set the option for future use when done formatting it.
 */
function drush_deploy_get_setting($name = NULL) {
  if (is_null($name)) {
    return;
  }
  $args = drush_get_arguments();
  $command = array_shift($args);
  switch($command) {
    case 'dec':
    case 'deploy-code':
      unset($command);
      switch($name) {
        case 'origin':
          // Expand abbreviated options in the $alias...
          $alias = array_shift($args);
          $origin = drush_deploy_alias_location($alias);
          unset($args, $command);
          /*
           * There are three options for the return of
           * drush_custom_alias_location(), check for two of them and set an
           * error if neither. Do the simplest first.
           */
          if (is_array($origin)) {
            unset($alias);
            drush_set_option('origin',$origin);
            drush_log('Origin alias loaded successfully.');
            return;
          }
          // Now do the one with a little more logic needed.
          if ($origin == 'parent') {
            $alias .= '.' . drush_get_option('origin', 'dev');
            $origin = drush_deploy_alias_location($alias);
            if (is_array($origin)) {
              drush_set_option('origin',$origin);
              drush_log('Origin alias loaded successfully.');
              return;
            }
          }
          // Now an error.
          return drush_set_error('Origin alias is invalid.');
        case 'target':
          $origin = drush_get_option('origin',NULL);
          $target = (isset($args[1]) ? $args[1] : drush_get_option('target','demo'));
          // Check if $orign was a set of sites, and that $target is not a specific alias.
          if (isset($origin['#group']) && strpos($target, '.') === FALSE) {
            unset($origin);
            $alias = array_shift($args) . '.' . $target;
            $target = drush_deploy_alias_location($alias);
            unset($alias);
            if (is_array($target)) {
              drush_set_option('target', $target);
              unset($target);
              drush_log('Target alias loaded successfully.');
              return;
            }
            return drush_set_error('Target alias not valid. Aborting.');
          } else { // if $target is specific, or no site-list to worry about.
            unset($origin);
            $target = drush_deploy_alias_location($target);
            if (is_array($target)) {
              drush_set_option('target', $target);
              unset($target);
              drush_log('Target alias loaded successfully.');
              return;
            }
            return drush_set_error('Invalid target alias.');
          }
        case 'items':
          $starting = $items = drush_get_option_list('items', 'modules,themes,libraries');
          if (empty($items)) {
            return drush_set_error('Items option must not be empty.');
          }
          $origin = drush_get_option('origin');
          foreach($items as $key => $item) {
            if (!file_exists($origin['location'] . $item)) {
              unset($items[$key]);
            }
          }
          $removed = array_diff($starting, $items);
          if (!empty($removed) && !empty($items)) {
            $msg = "The following items were not found in the origin directory,\n"
                   ."so they will not be copied:\n";
            $removed = implode(', ', $removed);
            $msg .= $removed;
            drush_log($msg,'warning');
            unset($msg);
          }
          $items = array_values($items);
          drush_set_option('items',$items);
          unset($items);
          return;
      }
      break;
    case 'ded':
    case 'deploy-download':
      unset($command);
      switch($name) {
        case 'core':
          // Check if the argument is an alias
          $core = $args[0];
          $alias = $core;
          if (substr($alias,0,1)!='@') {
            $alias = '@' . $alias;
          }
          $alias = drush_deploy_get_record($alias);
          // If not a valid alias...
          if (empty($alias) || !isset($alias['root'])) {
            if (is_numeric($core)) {
              // Confirm a valid destination is passed
              if (!($destination = drush_get_option('destination',FALSE))) {
                unset($destination, $alias, $core, $args);
                return drush_set_error('DEPLOY_NUMERIC_CORE_DESTINATION');
              }
              if (!is_dir($destination) && !is_link($destination)) {
                unset($destination, $alias, $core, $args);
                return drush_set_error('DEPLOY_DOWNLOAD_DESTINATION_EXIST');
              }
              // Set the core for future use.
              drush_set_option('core',$core);
              drush_deploy_drupal_version_available($core);
              unset($destination, $alias, $core, $args);
              return;
            }
            unset($alias, $core, $args);
            return drush_set_error('DEPLOY_DOWNLOAD_INVALID_CORE');
          }
          // Determine the destination from the alias.
          $destination = $alias['root'];
          if (substr($destination,-1) == '/') {
            $destination = substr($destination,0,-1);
          }
          if (($pos = strrpos($destination,'/'))!==FALSE) {
            $destination = substr($destination,0,$pos);
          }
          drush_set_option('destination',$destination);
          // Verify there is a need for this command.
          $version = drush_drupal_version($alias['root']);
          $pos = strrpos($version,'.');
          $core = substr($version,0,$pos);
          $minor = substr($version,$pos+1);
          if (drush_deploy_drupal_version_available($core, $minor)) {
            drush_set_option('core', $core);
            unset($args, $alias, $destination, $version, $pos, $core, $minor);
            return;
          }
          unset($args, $alias, $destination, $version, $pos, $core, $minor);
          return drush_set_error('DEPLOY_DOWNLOAD_RECOMMENDED');
          /**
          'symlinks' => 'Whether to establish symlinks or not, defaults to yes.',
      'items' => 'What items to establish symlinks to. Either an array can be provided, with keys being items in the Drupal root, targets being destinations to point to, or a list. List possibilities include htaccess,php.ini,sites,profiles. Defaults to list of sites and profiles.',
           */
        case 'symlinks':
          // Quickly handle the simplest case where links are turned off.
          if (!drush_get_option('symlinks',TRUE)) {
            drush_set_option('items', NULL);
            return;
          }
          $items = drush_get_option('items','sites,profiles');
          $skip = FALSE;
          // See if we need to create the array ourselves.
          if (is_string($items)) {
            $items = explode(',',$items);
            $possibilities = array(
              'sites',
              'profiles',
              'php.ini',
              'htaccess',
            );
            $items = array_intersect($items,$possibilities);
            unset($possibilities);
            $items = array_flip($items);
            if (isset($items['sites'])) {
              $items['sites'] = '../sites/<c>.x';
            }
            if (isset($items['profiles'])) {
              $items['profiles'] = '../profiles/<c>.x';
            }
            if (isset($items['htaccess'])) {
              $items['.htaccess'] = '../<c>_htaccess';
              unset($items['htaccess']);
            }
            if (isset($items['php.ini'])) {
              $items['php.ini'] = '../<c>_php.ini';
            }
            $skip = TRUE;
          }
          if (empty($items)) {
            drush_set_option('items',NULL);
          }

          $original = $items;
          $core = drush_get_option('core');
          $translate = array('<c>' => $core);
          foreach($items as $key => $value) {
            $items[$key] = dt($value, $translate);
          }
          $compare = array_diff($items, $original);
          if (!$skip && !empty($compare)) {
            drush_log("The <c> arguments have been substituted for the Drupal major version.", 'success');
          }
          drush_set_option('items', $items);
          unset($items, $skip, $original, $core, $translate);
          return;
      }
      break;
  }
}

/**
 * Confirms that there is a newer version of Drupal core recommended than the
 * existing one.
 *
 * @param numeric $core = '7'
 *   The version of Drupal-core to check.
 * @param numeric $minor = NULL
 *   The minor version to compare to.
 * @return varies
 *   If $minor == NULL:
 *     returns numeric of the recommended minor version.
 *   If there is a newer version than the $minor passed being recommended, TRUE
 *   Otherwise FALSE
 */
function drush_deploy_drupal_version_available($core = '7', $minor = NULL) {
  $cmd = 'pm-releases';
  $arg = array("drupal-$core");
  $alias = array('@none');
  $releases = drush_invoke_process($alias,$cmd,$arg);
  $recommended = $releases['object']['drupal']['recommended'];
  drush_set_option('version', $recommended);
  $pos = strrpos($recommended, '.');
  $remote_minor = substr($recommended,$pos+1);
  if (is_null($minor)) {
    return $remote_minor;
  }
  if ($minor < $remote_minor) {
    return TRUE;
  }
  return FALSE;
}

/**
 * A helper function to add a key of 'location' to an alias, pointing to the
 * site's folder for ease of use in manipulating files within that folder.
 * Also, takes care of the validation for custom-code while at it.
 *
 * @param string $alias
 *   An alias to load.
 * @return varies
 *   If a valid alias, will be returned with an additional key of location,
 *      indicating the location of the site folder.
 *   If the parent of other aliases, this will return a string of 'parent'.
 *   If not valid in any context, boolean of FALSE
 */
function drush_deploy_alias_location($alias = NULL) {
  $alias = (isset($alias) ? drush_deploy_get_record($alias) : drush_deploy_get_record() );
  if (isset($alias['uri'])) {
    if (isset($alias['root'])) {
      $location = $alias['root'];
      if (substr($location,-1) == '/') {
        $location = substr($location,0,-1);
      }
      $site_folder = $alias['uri'];
      if (($pos = strpos($site_folder,'//')) !== FALSE) {
        $site_folder = substr($site_folder,$pos+2);
      }
      if (substr($site_folder,-1) == '/') {
        $site_folder = substr($site_folder,0,-1);
      }
      $location .= '/sites/' . $site_folder . '/';
      $alias['location'] = $location;
      unset($location, $site_folder);
      return $alias;
    }
  }
  if (isset($alias['site-list'])) {
    return 'parent';
  }
  return FALSE;
}


/**
 * This function is a wrapper around drush_sitealias_get_record() and expands
 * the abbreviated options to make it easier to check for them in
 * code elsewhere. Also, drops empty values from the alias.
 *
 * @param string $alias = '@none'
 *   The alias to load.
 * @return array $alias
 *   The alias with all abbreviations in keys expanded to their long forms.
 */
function drush_deploy_get_record($alias = '@none') {
  $alias = drush_sitealias_get_record($alias);
  $mapping = array(
    'r'  => 'root',
    'l'  => 'uri',
    'v'  => 'verbose',
    'd'  => 'debug',
    'y'  => 'yes',
    'n'  => 'no',
    's'  => 'simulate',
    'p'  => 'pipe',
    'h'  => 'help',
    'ia' => 'interactive',
    'q'  => 'quiet',
    'c'  => 'config',
    'u'  => 'user',
    'b'  => 'backend',
  );
  foreach($alias as $key => $value) {
    if (empty($value)) {
      unset($alias[$key]);
    }
    else if (isset($mapping[$key])) {
      $alias[$mapping[$key]] = $value;
      unset($alias[$key]);
    }
  }
  return $alias;
}

/**
 * Implementation of drush_COMMANDNAME_validate().
 */
function drush_deploy_code_validate(){
  drush_deploy_get_setting('origin');
  if ($error = drush_get_error() != DRUSH_SUCCESS) {
    return $error;
  }
  drush_deploy_get_setting('target');
  if ($error = drush_get_error() != DRUSH_SUCCESS) {
    return $error;
  }
  drush_deploy_get_setting('items');
  if ($error = drush_get_error() != DRUSH_SUCCESS) {
    return $error;
  }
  // Check the confirmation options. Proceed if one is entered, prompt if not.
  $confirm['yes'] = drush_get_option(array('yes', 'y'), FALSE);
  $confirm['no'] = drush_get_option(array('no', 'n'), FALSE);

  if ($confirm['yes'] || $confirm['no']) {
    if ($confirm['no']) {
      return drush_user_abort('An option passed implies not the execute this.');
    }
    return;
  }
  // Now load the options for a final confirmation from the user.
  $items = drush_get_option('items');
  // Trim the trailing '/'s off of the locations for clarity.
  $origin = drush_get_option('origin');
  $origin = substr($origin['location'],0,-1);
  $target = drush_get_option('target');
  $target = substr($target['location'],0,-1);
  $msg = "\tWould you verify you want to copy these items:\n";
  $msg .= implode(', ', $items) . "\n";
  $msg .= "\tFrom: " . $origin . "\n";
  $msg .= "\t  To: " . $target . "\n";
  $msg .= "This  will overwrite files or folders of the same name in the second location.\n";
  // Must have a reference to Spaceballs here.
  $msg .= "This is your last chance to push the cancellation button.\n";
  $msg .= "Continue?";
  unset($items, $origin, $target);
  if (!drush_confirm($msg)) {
    unset($msg);
    // Continue the Spaceballs reference, yet reman clear about what's happening.
    return drush_user_abort("Have a nice day!\nAborting.");
  }
  unset($msg);
}

/**
 * Implementation of drush_COMMANDNAME().
 */
function drush_deploy_code() {
  $items = drush_get_option('items');
  $origin = drush_get_option('origin');
  $origin = $origin['location'];
  $target = drush_get_option('target');
  $target = $target['location'];
  if (drush_deploy_code_exec($items,$origin,$target)) {
    drush_log("Action completed.", 'success');
  } else {
    drush_log("There was an error; check permissions on the site folders.");
  }
}

/**
 * Do the work of the call, expects a maximum number of items to be less than 10.
 *
 * @param array $items
 *   numeric keys => strings of the items to duplicate.
 * @param string $origin
 *   Path to the folder to copy these from.
 * @param string $target
 *   Path to the folder to copy these to.
 * @return Varies
 *   A drush error if more than 9 $items are passed.
 *   The int $status returned from command otherwise.
 */
function drush_deploy_code_exec($items,$origin,$target) {
  $count = count($items);
  $cmd = 'cp -r %s';
  $args = array();
  for($i = 0;$i < $count;$i++) {
    $cmd .= " %s";
    $args[] = "$origin". $items[$i];
  }
  $args[$count++] = $target;
  switch($count) {
    case 2:
      $status = drush_shell_exec($cmd, $args[0], $args[1]);
      break;
    case 3:
      $status = drush_shell_exec($cmd, $args[0], $args[1], $args[2]);
      break;
    case 4:
      $status = drush_shell_exec($cmd, $args[0], $args[1], $args[2], $args[3]);
      break;
    case 5:
      $status = drush_shell_exec($cmd, $args[0], $args[1], $args[2], $args[3], $args[4]);
      break;
    case 6:
      $status = drush_shell_exec($cmd, $args[0], $args[1], $args[2], $args[3], $args[4], $args[5]);
      break;
    case 7:
      $status = drush_shell_exec($cmd, $args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6]);
      break;
    case 8:
      $status = drush_shell_exec($cmd, $args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7]);
      break;
    case 9:
      $status = drush_shell_exec($cmd, $args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8]);
      break;
    case 10:
      $status = drush_shell_exec($cmd, $args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8], $args[9]);
      break;
    default:
      return drush_set_error("What are you doing with so many things in your sites folder?\n"
        ."If you're serious about this, break the items up and run them in smaller subsets.");
  }
  return $status;
}

/**
 * Implementation of drush_COMMAND_validate().
 */
function drush_deploy_download_validate() {
  drush_deploy_get_setting('core');
  if (($error = drush_get_error()) !== DRUSH_SUCCESS) {
    return $error;
  }
  drush_deploy_get_setting('symlinks');
  if (($error = drush_get_error()) !== DRUSH_SUCCESS) {
    return $error;
  }
  $items = drush_get_option('items');
  $destination = drush_get_option('destination');
  $core = drush_get_option('core');
  $msg = dt("This command will download the recommended release of drupal-<core>.\n", array('<core>' => $core));
  $msg .= dt("It will be at this path: <path>\n", array('<path>' => $destination));
  if (isset($items)) {
    $msg .= dt("And afterwords, the following symlinks will be established:\n");
    foreach($items as $key => $value) {
      $msg .= "\t$key --> $value\n";
    }
  }
  $msg .= "This is your last chance to push the cancellation button.\n";
  $msg .= "Continue?";

  if (!drush_confirm($msg)) {
    drush_user_abort("You pushed the cancellation button! Aborting.");
  }
}

/**
 * Implementation of drush_COMMAND().
 */
function drush_deploy_download() {
  $alias = array('@none');
  $cmd = 'pm-download';
  $core = drush_get_option('core');
  $version = drush_get_option('version');
  $args = array(
    'drupal-' . $core,
  );
  $options = array(
    'destination' => drush_get_option('destination'),
    'drupal-project-rename' => FALSE,
  );
  $results = drush_invoke_process($alias, $cmd, $args, $options);
  if ($results['error_status'] == '0') {
    drush_log(dt('Downloaded Drupal <version>', array('<version>' => $version)), 'success');
  }
  else {
    return drush_set_error('DEPLOY_DOWNLOAD_DOWNLOAD_FAILED');
  }
}

/**
 * Implementation of drush_COMMANDFILE_post_COMMAND().
 */
function drush_deploy_post_deploy_download() {
  $items = drush_get_option('items');
  drush_print("version is: " . drush_get_option('version'));
  if (isset($items)) {
    $path = drush_get_option('destination') . '/drupal-' . drush_get_option('version') . '/';
    $cmd[] = 'rm -rf %s'; // ($path . $link)
    $cmd[] = 'ln -s %s %s'; // $target ($path . $link)
    $command = '';
    foreach($cmd as $cmd) {
      $command .= " && $cmd";
    }
    $command = substr($command,4);
    $status = TRUE;
    foreach($items as $link => $target) {
      $link = $path . $link;
      if (drush_shell_exec($command, $link, $target, $link) == FALSE) {
        $status = FALSE;
      }
    }
    if ($status) {
      drush_log('Symlinks established correctly.', 'success');
    }
    else {
      drush_log('There was an error with the symlinks.', 'warning');
    }
  }
}